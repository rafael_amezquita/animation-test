//
//  PSATagScrollView.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/13/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSATagView.h"

@interface PSATagScrollView : UIScrollView

- (void) addTagWithLabel:(NSString *)label;
- (void) addTagsFromArray:(NSArray *)tags;

@end
