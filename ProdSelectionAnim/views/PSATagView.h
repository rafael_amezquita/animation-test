//
//  PSATagView.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/13/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PSATagView : UIView

@property (nonatomic, strong) UILabel *tagLabel;

- (id)initWithLabel:(NSString*)label;


@end
