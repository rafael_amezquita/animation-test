//
//  PSATagView.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/13/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSATagView.h"

@implementation PSATagView


- (id)initWithLabel:(NSString*)label
{
    CGRect frame = CGRectMake(0, 10, 100, 25);
    CGRect labelFrame = CGRectMake(4, 3, 75, 20);
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        self.layer.borderWidth = 1.0;
        _tagLabel = [[UILabel alloc] initWithFrame:labelFrame];
        [_tagLabel setText:label];
        _tagLabel.textColor = [UIColor blackColor];
        [self addSubview:_tagLabel];
        UIImage *close = [UIImage imageNamed:@"closeTag"];
        UIImageView *img_view = [[UIImageView alloc] initWithImage:close];
        img_view.frame = CGRectMake(self.frame.size.width - 16, 8, 15, 10);
        img_view.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:img_view];
    }
    return self;
}





@end
