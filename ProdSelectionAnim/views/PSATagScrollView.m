//
//  PSATagScrollView.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/13/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSATagScrollView.h"

@interface PSATagScrollView ()
    //private
    @property (nonatomic, strong) PSATagView *tag;
@end

@implementation PSATagScrollView
@synthesize tag;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        self.contentSize = CGSizeMake(self.frame.size.width * 1.7, self.frame.size.height);
    }
    return self;
}

- (void) addTagWithLabel:(NSString *)label
{
    int iterator = (int)([[self subviews] count] - 2);// is always setted with two sub views
    tag = [[PSATagView alloc] initWithLabel:label];
    CGRect newFrame = tag.frame;
    newFrame.origin.x = iterator*(tag.frame.size.width + 5);
    tag.frame = newFrame;

    [self addSubview:tag];
}

- (void) addTagsFromArray:(NSArray *)tags
{
    for (PSATagView *tagLabel in tags) {
        [self addSubview:tagLabel];
    }
}


@end
