//
//  PSAManageDictionary.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/19/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSAManagedDictionary.h"

@implementation PSAManagedDictionary

@synthesize evaluatedDictionary;

+ (id) manager
{
    static PSAManagedDictionary *dic = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dic = [[self alloc] init];
    });
    return dic;
}

@end
