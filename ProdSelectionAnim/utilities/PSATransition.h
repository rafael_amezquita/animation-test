//
//  PSATransition.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/14/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSATransition : NSObject <UIViewControllerAnimatedTransitioning>

@property(nonatomic, strong) UIViewController *from;
@property(nonatomic, strong) UIViewController *to;

@end
