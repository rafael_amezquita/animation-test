//
//  PSAManageDictionary.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/19/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSAManagedDictionary : NSObject

@property (nonatomic, strong) NSDictionary *evaluatedDictionary;

+ (id) manager;

@end
