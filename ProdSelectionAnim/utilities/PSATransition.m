//
//  PSATransition.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/14/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSATransition.h"

@implementation PSATransition
@synthesize from, to;

- (NSTimeInterval) transitionDuration:(id)transitionContext
{
    return 0.7f;
}
- (void) animateTransition:(id)transitionContext
{
    [[transitionContext containerView] addSubview:to.view];
    
    CGRect fullFrame = [[UIScreen mainScreen] bounds];
    to.view.frame = CGRectMake(fullFrame.size.width, 0, fullFrame.size.width, fullFrame.size.height);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^(void) {
        
        to.view.frame = CGRectMake(0, 0, fullFrame.size.width, fullFrame.size.height);
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:YES];
        
    }];
}


@end
