//
//  Subcategories.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Categories, Types;

@interface Subcategories : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Categories *current_category;
@property (nonatomic, retain) NSSet *types;
@end

@interface Subcategories (CoreDataGeneratedAccessors)

- (void)addTypesObject:(Types *)value;
- (void)removeTypesObject:(Types *)value;
- (void)addTypes:(NSSet *)values;
- (void)removeTypes:(NSSet *)values;

@end
