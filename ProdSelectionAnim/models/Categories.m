//
//  Categories.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Categories.h"
#import "Subcategories.h"


@implementation Categories

@dynamic name;
@dynamic subcategories;

@end
