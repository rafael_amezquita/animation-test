//
//  Subtypes.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Subtypes.h"
#import "Types.h"


@implementation Subtypes

@dynamic name;
@dynamic current_type;

@end
