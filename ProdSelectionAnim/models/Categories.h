//
//  Categories.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subcategories;

@interface Categories : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *subcategories;
@end

@interface Categories (CoreDataGeneratedAccessors)

- (void)addSubcategoriesObject:(Subcategories *)value;
- (void)removeSubcategoriesObject:(Subcategories *)value;
- (void)addSubcategories:(NSSet *)values;
- (void)removeSubcategories:(NSSet *)values;

@end
