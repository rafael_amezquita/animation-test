//
//  Types.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Types.h"
#import "Subcategories.h"
#import "Subtypes.h"


@implementation Types

@dynamic name;
@dynamic subtypes;
@dynamic current_subcategory;

@end
