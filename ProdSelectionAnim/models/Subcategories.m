//
//  Subcategories.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Subcategories.h"
#import "Categories.h"
#import "Types.h"


@implementation Subcategories

@dynamic name;
@dynamic current_category;
@dynamic types;

@end
