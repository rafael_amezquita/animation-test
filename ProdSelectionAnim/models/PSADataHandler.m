//
//  PSADataHandler.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/19/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSADataHandler.h"

@interface PSADataHandler ()

    @property (nonatomic, strong) NSDictionary *evaluatedMap;

@end

@implementation PSADataHandler
@synthesize evaluatedMap;

#pragma mark - initialization

- (id) init
{
    self = [super init];
    if (self) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"products" ofType:@"json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
        evaluatedMap = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
    }
    return self;
}

#pragma mark - product categories and types

- (NSDictionary *) getResultsMap
{
    [[PSAManagedDictionary manager] setEvaluatedDictionary:[evaluatedMap objectForKey:@"categories"]];
    evaluatedMap = [[PSAManagedDictionary manager] evaluatedDictionary];
    return evaluatedMap;
}

#pragma mark - product results

- (NSDictionary *) finalResults
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"results" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
    evaluatedMap = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
    
    
    [[PSAManagedDictionary manager] setEvaluatedDictionary:[evaluatedMap objectForKey:@"categories"]];
    evaluatedMap = [[PSAManagedDictionary manager] evaluatedDictionary];
    
    return evaluatedMap;
}


@end
