//
//  Types.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subcategories, Subtypes;

@interface Types : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *subtypes;
@property (nonatomic, retain) Subcategories *current_subcategory;
@end

@interface Types (CoreDataGeneratedAccessors)

- (void)addSubtypesObject:(Subtypes *)value;
- (void)removeSubtypesObject:(Subtypes *)value;
- (void)addSubtypes:(NSSet *)values;
- (void)removeSubtypes:(NSSet *)values;

@end
