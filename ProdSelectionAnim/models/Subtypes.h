//
//  Subtypes.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/25/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Types;

@interface Subtypes : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Types *current_type;

@end
