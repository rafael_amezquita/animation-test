//
//  PSADataHandler.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/19/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSAManagedDictionary.h"

@interface PSADataHandler : NSObject

- (NSDictionary *) getResultsMap;
- (NSArray *) finalResults;

@end
