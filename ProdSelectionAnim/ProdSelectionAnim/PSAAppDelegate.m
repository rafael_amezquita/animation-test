//
//  PSAAppDelegate.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/12/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSAAppDelegate.h"

@implementation PSAAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    PSAViewController *mainController = (PSAViewController *)self.window.rootViewController;
    mainController.managedObjectContext = [self manageObjectContext];
    
    PSADataHandler *data = [[PSADataHandler alloc] init];
    NSDictionary *map = data.getResultsMap;
    
    BOOL areTablesEmpty = [self tablesDataInspection];
    
    if (areTablesEmpty) {
        [self populatingTablesWithDataMap:map];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data Stack

- (NSManagedObjectContext *) manageObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ProdSelectionAnim.sqlite"];
    NSLog(@"database url  = %@", storeURL);
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Data management

- (BOOL) tablesDataInspection
{
    NSManagedObjectContext *context = [self manageObjectContext];
    
    NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Categories" inManagedObjectContext:context];
    [fetch_request setEntity:entity];
    NSError *error;
    NSArray *results = [context executeFetchRequest:fetch_request error:&error];
    
    if ([results count] > 0) {
        return NO;
    } else {
        return YES;
    }
}

- (void) populatingTablesWithDataMap:(NSDictionary *)map
{
    NSArray *categories = [map allKeys];
    
    NSManagedObjectContext *context = [self manageObjectContext];

    NSArray *aux_data;
    NSDictionary *aux_map;
    
    //adding categories
    for (NSString *cat_name in categories) {
        
        Categories *category = [NSEntityDescription insertNewObjectForEntityForName:@"Categories" inManagedObjectContext:context];
        category.name = cat_name;
        aux_data = [[map objectForKey:cat_name] allKeys];
        
        //adding subcategories
        for (NSString *subcat_name in aux_data) {
            Subcategories *subcategory = [NSEntityDescription insertNewObjectForEntityForName:@"Subcategories" inManagedObjectContext:context];
            subcategory.name = subcat_name;
            aux_map = [[map objectForKey:cat_name] objectForKey:subcat_name];
            aux_data = [aux_map allKeys];
            [category addSubcategoriesObject:subcategory];
            
            //adding types
            for (NSString *type_name in aux_data) {
                
                Types *type = [NSEntityDescription insertNewObjectForEntityForName:@"Types" inManagedObjectContext:context];
                type.name = type_name;
                aux_data = [NSArray arrayWithArray:[aux_map objectForKey:type_name]];
                [subcategory addTypesObject:type];
                
                //adding subtypes
                for (NSString *subtype_name in aux_data) {
                    Subtypes *subtype = [NSEntityDescription insertNewObjectForEntityForName:@"Subtypes" inManagedObjectContext:context];
                    subtype.name = subtype_name;
                    [type addSubtypesObject:subtype];
                }
            }
        }
    }
    
    //saving context
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Woops couldn't save = %@", [error localizedDescription]);
    }
}

@end
