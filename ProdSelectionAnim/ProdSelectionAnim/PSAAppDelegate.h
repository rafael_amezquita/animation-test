//
//  PSAAppDelegate.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/12/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Categories.h"
#import "Subcategories.h"
#import "Types.h"
#import "Subtypes.h"
#import "PSADataHandler.h"
#import "PSAViewController.h"

@interface PSAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
