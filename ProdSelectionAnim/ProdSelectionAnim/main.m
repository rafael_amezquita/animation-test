//
//  main.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/12/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PSAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PSAAppDelegate class]));
    }
}
