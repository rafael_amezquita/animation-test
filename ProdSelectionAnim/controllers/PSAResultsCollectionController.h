//
//  PSAResultsCollectionController.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/20/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSATagScrollView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PSAResultsCollectionController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet PSATagScrollView *tagsScrollView;
@property (nonatomic, strong) NSArray *tagsArray;

@end
