//
//  PSAResultsCollectionController.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/20/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSAResultsCollectionController.h"

@interface PSAResultsCollectionController ()

    @property (nonatomic, strong) NSArray *result;
    @property (nonatomic, weak) UIViewController *homeController;

@end

@implementation PSAResultsCollectionController

#pragma mark -lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSLog(@"tags = %@", _tagsArray);
    [_tagsScrollView addTagsFromArray:_tagsArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_result count];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    /*
    UILabel *title = (UILabel *)[cell viewWithTag:100];
    title.text = ((PSAResultModel *)[_result objectAtIndex:indexPath.row]).title;
    
    UILabel *sub = (UILabel *)[cell viewWithTag:99];
    sub.text = ((PSAResultModel *)[_result objectAtIndex:indexPath.row]).subtitle;

    NSString *url = ((PSAResultModel *)[_result objectAtIndex:indexPath.row]).thumb_url;
    UIImageView *img_view = (UIImageView *)[cell viewWithTag:98];
    [img_view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [img_view.layer setBorderWidth:2.0f];
    
    [img_view sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"pic"]];
    
    [cell.layer setBorderWidth:1.0f];
    [cell.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    */
    return cell;
}

#pragma mark - IBActions

- (IBAction)backHome:(id)sender
{
    _homeController = [self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    [self presentViewController:_homeController animated:YES completion:nil];
}

@end
