//
//  PSAViewController.m
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/12/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "PSAViewController.h"

@interface PSAViewController ()

    @property (nonatomic, strong) NSMutableArray *currentData;
    @property (nonatomic, strong) NSArray *categories;
    @property (nonatomic, readwrite) BOOL ending;
    @property (nonatomic, strong) PSAResultsCollectionController *results_collection;
    @property (nonatomic, strong) PSATransition *transition;
    @property (nonatomic, strong) NSMutableArray *selectedCells;

@end

@implementation PSAViewController

#pragma mark - lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //initial data
    _categories = [self selectCategories];

    //table data array and higlighted cells array initialization
    _currentData = [[NSMutableArray alloc] initWithArray:_categories];
    _selectedCells = [[NSMutableArray alloc] init];
    
    _ending = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CoreData auxiiars

- (NSArray *) selectCategories
{
    NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
    Categories *entity = (Categories *)[NSEntityDescription entityForName:@"Categories" inManagedObjectContext:_managedObjectContext];
    [fetch_request setEntity:(NSEntityDescription *)entity];
    NSError *error;
    NSArray *categories = [_managedObjectContext executeFetchRequest:fetch_request error:&error];
    NSMutableArray *cat_names = [[NSMutableArray alloc] init];
    for (Categories *cat in categories) {
        if ([cat isKindOfClass:[Categories class]]) {
            [cat_names addObject:cat];
        }
    }
    return cat_names;
}

- (NSArray *) selectSubCategoriesWhereCat:(NSEntityDescription *)cat
{
    NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Subcategories" inManagedObjectContext:_managedObjectContext];
    [fetch_request setEntity:entity];
    [fetch_request setPredicate:[NSPredicate predicateWithFormat:@"current_category == %@", (Categories *)cat]];
    //[fetch_request setSortDescriptors:[NSSortDescriptor sortDescriptorWithKey:@"uuid" ascending:NO]];
    NSError *error;
    NSArray *subcategories = [_managedObjectContext executeFetchRequest:fetch_request error:&error];

    NSMutableArray *subcats = [[NSMutableArray alloc] init];
    for (Subcategories *sub in subcategories) {
        NSLog(@"subcategories = %@", sub.name);
        [subcats addObject:sub];
    }

    return subcats;
}

- (NSArray *) selectTypesWhereSubcat:(NSEntityDescription *)subcat
{
    NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Types" inManagedObjectContext:_managedObjectContext];
    [fetch_request setPredicate:[NSPredicate predicateWithFormat:@"current_subcategory == %@", (Subcategories *)subcat]];
    [fetch_request setEntity:entity];
    NSError *error;
    NSArray *result_types = [_managedObjectContext executeFetchRequest:fetch_request error:&error];

    NSMutableArray *types = [[NSMutableArray alloc] init];
    for (Types *type in result_types) {
        NSLog(@"types = %@", type.name);
        [types addObject:type];
    }
    
    return types;
}

- (NSArray *) selectSubtypesWhereType:(NSEntityDescription *)type
{
    NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Subtypes" inManagedObjectContext:_managedObjectContext];
    [fetch_request setPredicate:[NSPredicate predicateWithFormat:@"current_type == %@", (Types *)type]];
    [fetch_request setEntity:entity];
    NSError *error;
    NSArray *subtypes = [_managedObjectContext executeFetchRequest:fetch_request error:&error];
    
    for (NSString *subtype in subtypes) {
        NSLog(@"subtype = %@", subtype);
        
    }
    
    return subtypes;
}

#pragma mark - UITableViewDataSource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_currentData count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"categories";
    UITableViewCell *cell;
    UIColor *bg_color = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        //fill selected cells background
        if (!_ending) {
            int selectedCells = (int)[_selectedCells count];
            for (int i = 0; i < selectedCells; i++) {
                if (indexPath.row == i) {
                    [self cellColorHandler:cell withColor:bg_color];
                }
            }
        }

    }
    
    int index = (int) indexPath.row;
    NSString *currentString;
    if ([[_currentData objectAtIndex:index] isKindOfClass:[NSString class]]) {
        currentString = [_currentData objectAtIndex:index];
    } else {
        currentString = [((NSManagedObject *)[_currentData objectAtIndex:index]) valueForKey:@"name"];
    }
    cell.textLabel.text = currentString;
    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //setting cell's style
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    cell.textLabel.textColor = [UIColor whiteColor];

    
    //1. add cell into selected  array
    [_selectedCells addObject:cell.textLabel.text];
    if ([_selectedCells count] == 4) _ending = YES;
    if (_ending) {
        [self changeController];
        return;
    }
    
    
    //2. handle the new data
    //adding the correspondednt tag
    [_tagScroll addTagWithLabel:cell.textLabel.text];
    
    //model handler: is needed a diferent model depending the deep level
    int distance_from_top = 0;
    int level = (int)[_selectedCells count];
    int delta = level - 1;
    distance_from_top = cell.frame.size.height * delta;
    
    //3. ordering and animate the new cells
    //reloading the table data
    [self reloadDataWithSelection:indexPath /*andEvaluatedModel:model*/];
    //fade out cells
    [self fadeOutCellsInTable:_productsTable withoutThoseInArray:_selectedCells];
    //cell animation
    [self animateCurrentCell:cell inTable:_productsTable withYDistance:distance_from_top];
    
 }

#pragma mark - utility methods

/*!
 * Method in charge fill the corresponden cell in the tableView
 * Call from:  tableView:cellForRowAtIndexPath: method.
 * \param cell The current cell 
 * \param bg_color background color
 */
- (void) cellColorHandler:(UITableViewCell *) cell withColor:(UIColor *)bg_color
{
    cell.backgroundColor = bg_color;
    cell.textLabel.textColor = [UIColor whiteColor];
}

/*!
 * Method in charge to load a data model depending the current deep 
 * \param selection The current cell label text
 * \param model The current data model
 */
- (void) reloadDataWithSelection:(NSIndexPath *)index /*andEvaluatedModel:(id)model*/
{
    NSArray *arrayToPass;
    NSEntityDescription *entity = [_currentData objectAtIndex:index.row];
    if ([_selectedCells count] == 1) {
        arrayToPass = [self selectSubCategoriesWhereCat:entity];
    } else if ([_selectedCells count] == 2) {
        arrayToPass = [self selectTypesWhereSubcat:entity];
    } else if ([_selectedCells count] == 3) {
        arrayToPass = [self selectSubtypesWhereType:entity];
    }
    
    [_currentData removeAllObjects];
    [_currentData addObjectsFromArray:_selectedCells];
    [_currentData addObjectsFromArray:arrayToPass];
}

/*!
 * Method in charge to handle the alpha value from the table view cells
 * \param tableView The current table
 * \param exceptions An array with the cells to keep apha value in 1
 */
- (void) fadeOutCellsInTable:(UITableView *)tableView withoutThoseInArray:(NSArray *)exceptions
{
    NSArray *visibleCells = [tableView visibleCells];
    
    long i, j;
    long cells = [visibleCells count];
    long blues = [exceptions count];
    for (i = 0; i < cells; i++) {
        NSString *cell_text = ((UITableViewCell*)[visibleCells objectAtIndex:i]).textLabel.text;
        bool isEqual = false;
        for (j = 0; j < blues; j++) {
            NSString *blue_text = [exceptions objectAtIndex:j];
            if (cell_text == blue_text) {
                isEqual = true;
            }
        }
        if (isEqual) {
            ((UITableViewCell*)[visibleCells objectAtIndex:i]).alpha = 1;
        } else {
            ((UITableViewCell*)[visibleCells objectAtIndex:i]).alpha = 0;
        }
        
    }
}

/*!
 * Method in charge to animate the table cell
 * \param cell The current table cell
 * \param tableView The current table
 * \param ydistance Final cell distance from the top
 */
- (void) animateCurrentCell:(UITableViewCell *)cell inTable:(UITableView *)tableView withYDistance:(int)ydistance
{
    [UIView animateWithDuration:0.7 animations:^(void){
        //getting uitableviewcell up to top
        cell.frame = CGRectMake(cell.frame.origin.x, ydistance, cell.frame.size.width, cell.frame.size.height);
    } completion:^(BOOL finished) {
        [tableView reloadData];
    }];
}

/*!
 * Method in charge to change the to Results contoller
 */
- (void) changeController
{
    _results_collection = [self.storyboard instantiateViewControllerWithIdentifier:@"results_collection"];
    _results_collection.transitioningDelegate = self;
    NSArray *tagsInScroll = [_tagScroll subviews];
    NSMutableArray *tagBridge = [[NSMutableArray alloc] init];
    for (PSATagView *item in tagsInScroll) {
        if ([item isKindOfClass:[PSATagView class]]) {
            //[_results_collection.tagsScrollView addTagWithLabel:item.tagLabel.text];
            [tagBridge addObject:item];
        }
    }
    
    _results_collection.tagsArray = tagBridge;

    [self presentViewController:_results_collection animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate methods

- (id < UIViewControllerAnimatedTransitioning >)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    _transition = [[PSATransition alloc] init];
    _transition.from = self;
    _transition.to = _results_collection;
    return _transition;
}




@end
