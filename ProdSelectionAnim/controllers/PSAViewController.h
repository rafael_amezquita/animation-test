//
//  PSAViewController.h
//  ProdSelectionAnim
//
//  Created by Rafael Andres Amezquita Mejia on 8/12/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSATagScrollView.h"
#import "PSATransition.h"
#import "PSAResultsCollectionController.h"

#import "Categories.h"
#import "Subcategories.h"
#import "Types.h"

#define RED     0.2
#define GREEN   0.8
#define BLUE    1

@interface PSAViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, weak) IBOutlet UITableView *productsTable;
@property (nonatomic, weak) IBOutlet PSATagScrollView *tagScroll;

@end
